// ==================================
// ESLINT-SETTING - don't DEL comment's!!!
// ==================================
/* eslint no-undef: "error" */
/* eslint-env node */
// ==================================


/* ==== IMPORT PARAMS ==== */
'use strict';
import { task, src, dest, lastRun, series, parallel, watch } from 'gulp';
import { gifsicle, jpegtran, optipng } from 'gulp-imagemin';
import webpackStream from 'webpack-stream';
import resolver from 'stylus';
import log from 'fancy-log';
import path from 'path';
import del from 'del';
import fs from 'fs';
/* ==== ----- ==== */


/* ==== NODE.JS LISTENER FIX ==== */
import Events from 'events';
Events.defaultMaxListeners = 100;
/* ==== ----- ==== */


/* ==== GULP PLUGIN LOADER ==== */
const iniLoader = {
		rename: {
			'gulp-iconfont-css': 'iconfontCss',
			'gulp-babel-minify': 'babelMinify',
			'gulp-file-include': 'include',
			'gulp-css-url-adjuster': 'urlReplace',
			'gulp-string-replace': 'htmlReplace',
			'gulp-svg-sprite': 'svgSprite',
			'gulp-svg-symbols': 'svgSymbols'
		}
	},
	_run = require('gulp-load-plugins')(iniLoader),
	isDevelopment = !process.env.NODE_ENV
	|| process.env.NODE_ENV === 'development',
	isPublic = process.env.NODE_ENV === 'public';
/* ==== ----- ==== */


/* ==== PLUGIN LIST SEE IN PACKAGE.JSON ==== */
const
	browserSync = require('browser-sync').create(),
	combiner = require('stream-combiner2').obj,
	webpack = webpackStream.webpack;
/* ==== ----- ==== */


/* ==== Sources and directions for files ==== */
const
	inDev = 'development',
	inDevApps = 'development/components',
	inPub = 'public',
	inPubCss = inPub + '/css',
	inPubJs = inPub + '/js',
	dir = __dirname + '/';
/* ==== ----- ==== */


// /* ==== CONFIG ==== */
// const __cfg = fs.readFileSync('gulpfile.config', 'utf-8');
// console.log(__cfg);
// /* ==== ----- ==== */


/* ==== REPLACE ==== */
const
	cssImageLink = '../media/img/',
	cssFontsLink = { replace: ['../media/img/fonts/', '../media/fonts/'] },
	htmlSrcReplace = { in: 'src="./components/img/', out: 'src="../media/img/' },
	imageClearPath = { in: /^development\\components\\/gi, out: 'public\\media\\', },
	// iconfontReplace = { in: /(?:.DUMMYTEXT-|:before)/g, out: '$' },
	webpackAddCustomPlugins = {
		// picker: path.resolve('./node_modules/pickadate/lib/picker'),
	};
/* ==== ----- ==== */


/* ==== ----- ==== */
const __cfg = {

	copyPlugins: {
		init: {
			arr: [
				// CSS
				dir + 'node_modules/normalize.css/normalize.css',
				dir + 'node_modules/lite-padding-margin/dist/css-source/lite-padding-margin.min.css',
				// dir + 'node_modules/owl.carousel/dist/assets/owl.carousel.min.css',
				// dir + 'node_modules/owl.carousel/dist/assets/owl.theme.default.min.css',
				// dir + 'node_modules/fullpage.js/dist/jquery.fullpage.min.css',
				// MAP
				// dir + 'node_modules/fullpage.js/dist/jquery.fullpage.min.css.map'
			]
		}
	},
	seoStuff: {
		init: {
			arr: [
				`${inDevApps}/favicon.{png,ico}`,
				`${inDevApps}/robots.txt`,
				`${inDevApps}/.htaccess`,
			]
		}
	},

	browserSync: {
		init: {
			server: {
				baseDir: inPub,
				middleware: function(req, res, next) {
					res.setHeader('Access-Control-Allow-Origin', '*');
					next();
				}
			},
			ghostMode: false,
			notify: false,
			open: false,
			httpModule: 'spdy',
			port: 3000,
			https: true,
			'browser': 'chrome.exe'
		}
	},
	include: {
		init: {
			prefix: '<!-- @@',
			suffix: '-->',
			basepath: '@file'
		}
	},
	stylus: {
		sprite: {
			init: {
				import: __dirname + `/${inDev}/tmp/sprite`,
				define: { url: resolver() }
			},
			svg: {
				init: {
					mode: {
						css: {
							dest: '.',
							bust: !isDevelopment,
							sprite: 'sprite.svg',
							layouts: 'vertical',
							prefix: '$',
							dimensions: true,
							render: { styl: { dest: 'sprite.styl' } }
						}
					}
				}
			}
		}
	},
	icon: {
		css: {
			init: {
				fontName: 'Glyphs',
				path: `${inDev}/components/img/__glyphs/config.css`,
				targetPath: dir + `${inDev}/tmp/glyphs.styl`,
				cssClass: '$',
			}
		},
		font: {
			init: {
				fontName: 'Glyphs',
				prependUnicode: false,
				formats: ['woff2', 'ttf'],
				normalize: true,
				fontHeight: 1001,
				timestamp: Math.round(Date.now() / 1000),
			}
		}
	},
	imagemin: {
		init: {
			arr: [
				gifsicle({ interlaced: true }),
				jpegtran({ progressive: true }),
				optipng({ optimizationLevel: 5 })
			]
		}
	},
	webpack: {
		plugins: {
			init: {
				'$': 'jquery',
				'jQuery': 'jquery',
				'window.jQuery': 'jquery',
				// 'owl.carousel': 'owl.carousel',
			}
		},
		setting: {
			init: {
				stats: {
					colors: true,
					modules: true,
					reasons: true,
					errorDetails: true
				}
			}
		},
		uglify: {
			init: {
				cacheFolder: path.resolve(__dirname, `${inDevApps}/js/cached_uglify/`),
				debug: false,
				minimize: true,
				sourceMap: true,
				mangle: true,
				compress: {
					warnings: false, // Suppress uglification warnings
					pure_getters: true,
					unsafe: true,
					unsafe_comps: true,
					screw_ie8: true
				},
				output: { comments: false, },
				// PLUGIN IGNORE LIST
				exclude: [/\.min\.js$/gi, 'global.js']
			}
		}
	}
};
/* ==== ----- ==== */


/* ==== OPTIONAL ==== */
const
	errorConfig = (name, descript, err) => {
		return {
			title: `${name} - ${descript}`,
			message: 'Файл: \n\n' + err.message + '\n',
			sound: false
		};
	},

	imageClearCache = (filepath) => {
		let str = filepath;
		log('filepath origin', filepath);
		str = str.replace(imageClearPath.in, imageClearPath.out);
		log('filepath result', str);
		fs.unlink(str, (err) => {
			if (err) throw err;
			log('Deleted file: ', path.basename(filepath));
		});
	};
/* ==== ----- ==== */




// === Install Plugins
task('plugins:copy', () => 
	combiner(
		src(__cfg.copyPlugins.init.arr, { since: lastRun('plugins:copy') }),
		_run.newer(`${inDevApps}/plugins`),
		dest(`${inDevApps}/plugins`))
);




// === Move files
// * HTML
task('html:move', () => 
	combiner(
		src(`${inDev}/*.html`, { since: lastRun('html:move') }),
		_run.cached('html:move'),
		_run.htmlReplace(htmlSrcReplace.in, htmlSrcReplace.out),
		_run.include(__cfg.include.init),
		_run.remember('html:move'),
		_run.newer(inPub),
		dest(inPub)).on('error',
		_run.notify.onError((err) => errorConfig('HTML', 'ошибка include', err)))
);
// * HTML add html parts path
task('html:move:layouts', () => 
	combiner(
		src(`${inDev}/*.html`),
		_run.include(__cfg.include.init),
		dest(inPub)).on('error',
		_run.notify.onError((err) => errorConfig('layouts', 'ошибка include', err)))
);
// * HTML replace html urls
task('html:move:urlreplace', () => 
	combiner(
			src(`${inDev}/*.html`, { since: lastRun('html:move:urlreplace') }),
			_run.htmlReplace(htmlSrcReplace.in, htmlSrcReplace.out),
			dest(inDev))
		&& combiner(
			src(`${inDev}/layouts/**/*.html`),
			_run.htmlReplace(htmlSrcReplace.in, htmlSrcReplace.out),
			dest(`${inDev}/layouts`)
		).on('error',
			_run.notify.onError((err) => errorConfig('Urlreplace', 'ошибка include', err)))
);


// === Build NODE content
// * Fonts
task('fonts:loading', () => 
	combiner(
		src(`${inDevApps}/fonts/**/*.{ttf,eot,otf,woff,woff2}`, ''),
		_run.newer(`${inPub}/media/fonts`),
		dest(`${inPub}/media/fonts`))
);
// * SEO-stuff
task('seo:stuff', () => 
	combiner(
		src(__cfg.seoStuff.init.arr, { since: lastRun('seo:stuff') }),
		_run.newer(`${inPub}/`),
		dest(`${inPub}/`)).on('error',
		_run.notify.onError((err) => errorConfig('SEO-stuff', 'файл не найден', err)))
);
// * CSS
task('plugins:move:css', () => 
	combiner(
		src(`${inDevApps}/plugins/**/*.css`),
		_run.newer(inPubCss),
		_run.concat('plugins.css'),
		dest(inPubCss))
);
// * MAP
task('plugins:move:map', () => 
 combiner(
		src(`${inDevApps}/plugins/**/*.map`),
		_run.newer(inPubCss),
		dest(inPubCss))
);
// * JS
task('plugins:move:js', () => 
	combiner(
		src(`${inDevApps}/plugins/**/*.js`),
		_run.newer(inPubJs),
		_run.concat('plugins.js'),
		dest(inPubJs))
);
// * METRICA
task('plugins:move:metrica', () =>
	combiner(
		src(`${inDevApps}/metrica/**/*.js`),
		_run.newer(inPubJs),
		_run.concat('metrica.js'),
		dest(inPubJs))
	);

// === Stylus params
task('stylus:loading', () => 
	combiner(
		src(`${inDevApps}/stylus/**/connect.styl`),
		_run.if(isDevelopment, _run.sourcemaps.init()),
		_run.stylus(__cfg.stylus.sprite.init),
		_run.autoprefixer({ browsers: ['last 5 version'] }),
		_run.if(isDevelopment, _run.sourcemaps.write()),
		_run.urlReplace({ prepend: cssImageLink }),
		_run.urlReplace(cssFontsLink),
		_run.rename('global.css'),
		dest(inPubCss)).on('error',
		_run.notify.onError((err) => errorConfig('Stylus', 'ошибка синтаксиса', err)))
);


// === Webpack params
task('webpack', (flag) => {
	const path = require('path');
	const PATHS = {
		root: path.join(__dirname, ''),
		app: path.join(__dirname, `${inDevApps}/js/`),
	};

	let
		firstRun = true,
		init = {
			entry: { global: path.join(__dirname, `${inDevApps}/js/global`) },
			output: {
				path: PATHS.root,
				filename: '[name].js',
				sourceMapFilename: '[name].map'
			},
			stats: __cfg.webpack.setting.init.stats,
			watch: isDevelopment,
			devtool: isDevelopment ? 'cheap-module-inline-source-map' : false,
			module: {
				rules: [{
					test: /\.js$/,
					exclude: /node_modules/,
					use: {
						loader: 'babel-loader',
						options: { presets: ['@babel/preset-env'] }
					}
				}]
			},
			resolve: {
				modules: ['node_modules'],
				extensions: ['.js'],
				alias: webpackAddCustomPlugins
			},
			plugins: [
				new webpack.DefinePlugin(isDevelopment),
				new webpack.optimize.OccurrenceOrderPlugin(),
				new webpack.NoEmitOnErrorsPlugin(),
				new webpack.ProvidePlugin(__cfg.webpack.plugins.init),
				new webpack.optimize.UglifyJsPlugin(__cfg.webpack.uglify.init),
				new webpack.optimize.CommonsChunkPlugin({
					name: 'plugins',
					filename: 'plugins.js',
					minChunks: module => module.context && module.context.indexOf('node_modules') !== -1
				}) 
			],
		};
	return combiner(
		src(`${inDevApps}/js/**/*.js`), webpackStream(init),
		dest(inPubJs).on('data', () => firstRun ? flag() : null || ''));
});


// === All media GRATHIC - jpg svg png
// * jpg, jpeg, png, gif
task('img:all', () => 
	combiner(
		src(`${inDevApps}/img/**/*.{jpg,jpeg,png,gif}`),
		_run.if(isPublic, _run.filesize()),
		dest(`${inPub}/media/img`))
);
// * __sprite svg
task('img:svg', () => 
	combiner(
		src(`${inDevApps}/img/__sprite/**/*.svg`, { allowEmpty: true }),
		_run.svgSprite(__cfg.stylus.sprite.svg.init),
		_run.if('*.styl',
			dest(`${inDev}/tmp`),
			dest(`${inPub}/media/img`)))
);
// * __glyphs svg
task('img:tosvg', () => 
	combiner(
		src(`${inDevApps}/img/__glyphs/**/*.svg`, { allowEmpty: true }),
		_run.svgmin({
			js2svg: {
				pretty: true
			},
			plugins: [
				{ removeDoctype: true, },
				{ removeXMLProcInst: true, },
				{ removeTitle: true, },
				{ removeDesc: { removeAny: true, }, },
				{ convertTransform: {}, },
			],
		}),
		_run.cheerio({
			run: ($) => {
				// 'fill',  <- color fill attr for svg sprte 
				let arr = ['stroke', 'style'];
				arr.forEach((item) => {
					$(`[${item}]`).removeAttr(item);
				});
			},
			parserOptions: { xmlMode: true }
		}),
		_run.replace('&gt;', '>'),
		_run.svgSymbols({
			slug: {
				separator: '-',
			},
			id: 'id-glyphs-%f',
			class: '$%f$',
			templates: [
				'default-svg',
				'default-stylus',
			],
			svgAttrs: {
				class: 'inline-svg__init',
				'aria-hidden': 'true',
				version: '1.1'
			}
		}),
		dest(`${inDev}/tmp`))
);


// === Compress IMG
task('img:compress', () => 
	combiner(
		src(`${inDevApps}/img/**/*.{jpg,jpeg,png,gif}`),
		_run.newer(`${inPub}/media/img/`),
		_run.cached('imgcache'),
		_run.if(isPublic, _run.imagemin(__cfg.imagemin.init.arr)),
		_run.if(isPublic, _run.filesize()),
		_run.remember('imgcache'),
		dest(`${inPub}/media/img`))
);


// === Minification bundls
// * CSS
task('mini:css', () => 
	combiner(
		src(`${inPub}/css/global.css`, { allowEmpty: true }),
		_run.if(isPublic, _run.cssnano()),
		_run.rename({ suffix: '.min' }),
		dest(inPubCss))
);
// * JS
task('mini:js', () => 
	combiner(
		src(`${inPub}/js/global.js`, { allowEmpty: true }),
		_run.if(isPublic, _run.babelMinify()),
		_run.rename({ suffix: '.min' }),
		dest(inPubJs))
);


// === Clean Public folder
task('deleteFiles', () =>
	del([inPub, dir + `${inDevApps}/plugins/`], { read: false })
);


// === Server params
task('server:loading', () => {
	browserSync.init(__cfg.browserSync.init, (err, bs) => {
		bs.addMiddleware('*', (req, res) => {
			res.write(fs.readFileSync(path.join(__dirname, `${inPub}/404.html`)));
			res.end();
		});
	}), 
	browserSync.watch(`${inPub}/**/*.*`)
		.on('change', browserSync.reload);
});


// === Watch all files
task('watchme', () => {
	// * watch Dev
	watch(`${inDev}/*.html`, series('html:move'));
	watch(`${inDevApps}/img/**/*.{jpg,jpeg,png,gif}`, series('img:all'))
		.on('unlink', (filepath) => imageClearCache(filepath));
	watch(`${inDevApps}/fonts/**/*.{ttf,eot,otf,woff,woff2}`, series('fonts:loading'));
	watch(`${inDev}/layouts/**/*.html`, series('html:move:layouts'));
	watch(`${inDevApps}/plugins/**/*.{css}`, parallel('plugins:move:css'));
	watch([`${inDevApps}/stylus/**/*.{styl,stylus}`, `${inDev}/tmp/sprite.styl`], series('stylus:loading'));
	watch(`${inDevApps}/img/__sprite/**/*.svg`, series('img:svg'));
	watch(`${inDevApps}/img/__glyphs/**/*.svg`, series('img:tosvg'));
	// * watch Pub
	watch(`${inPub}/css/**/*.css`);
});

task('skip', (cb) => cb());

// === Gulp setup loading tasks
task('loadingGulpConfig', series(
	parallel('img:all', 'img:svg', 'img:tosvg'),
	parallel('seo:stuff', 'fonts:loading'),
	parallel('html:move', 'html:move:layouts'),
	parallel('html:move:urlreplace'),
	parallel('plugins:copy', 'stylus:loading'),
	parallel('plugins:move:css', 'plugins:move:map', 'plugins:move:metrica'),
	parallel('webpack')
));


// === Start gulp config - yarn dev
task('default',
	series(
		'deleteFiles',
		'loadingGulpConfig',
		series(isPublic ? parallel('mini:css', 'mini:js') : series('skip')),
		series(isDevelopment ? parallel('server:loading', 'watchme') : series('skip')),
	)
);